"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NavPluginRoot = exports._NavPluginRoot = exports.NavItems = undefined;

var _numberrange = require("../reducers/numberrange");

const React = qu4rtet.require("react"); // Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

const { Component } = React;
const { connect } = qu4rtet.require("react-redux");
const {
  Menu,
  MenuItem,
  MenuDivider,
  Dialog,
  Button,
  ButtonGroup,
  ContextMenu,
  RadioGroup,
  Radio
} = qu4rtet.require("@blueprintjs/core");
const { FormattedMessage } = qu4rtet.require("react-intl");
const { withRoute } = qu4rtet.require("react-router-dom");
const { TreeNode } = qu4rtet.require("./components/layouts/elements/TreeNode");
const classNames = qu4rtet.require("classnames");
const { pluginRegistry } = qu4rtet.require("./plugins/pluginRegistration");
const { DeleteDialog } = qu4rtet.require("./components/elements/DeleteDialog");
const { withRouter } = qu4rtet.require("react-router");

class _PoolItem extends Component {
  constructor(props) {
    super(props);

    this.poolHasRandom = () => {
      return this.props.pool.randomizedregion_set !== undefined;
    };

    this.poolHasListBased = () => {
      return this.props.pool.listbasedregion_set !== undefined;
    };

    this.goTo = path => {
      this.props.history.push(path);
    };

    this.toggleAllocation = () => {
      const { pool, serverID } = this.props;
      // redirect to pool regions if not already there.
      this.goTo(`/number-range/region-detail/${serverID}/${pool.machine_name}`);
      this.setState({ isAllocationOpen: !this.state.isAllocationOpen });
    };

    this.getAllowedRegionTypes = () => {
      const { pool } = this.props;
      if (pool.sequentialregion_set.length > 0) {
        return { sequential: true, randomized: false, listBased: false };
      } else if (this.poolHasRandom() && pool.randomizedregion_set.length > 0) {
        return { sequential: false, randomized: true, listBased: false };
      } else if (this.poolHasListBased() && pool.listbasedregion_set.length > 0) {
        return { sequential: false, randomized: false, listBased: true };
      }
      return {
        sequential: true,
        randomized: this.poolHasRandom(),
        listBased: this.poolHasListBased()
      };
    };

    this.goToEdit = evt => {
      let { pool } = this.props;
      ContextMenu.hide();
      this.props.history.push({
        pathname: `/number-range/edit-pool/${this.props.serverID}/${pool.machine_name}`,
        state: { defaultValues: this.props.pool, editPool: true }
      });
    };

    this.setAllocation = evt => {
      evt.preventDefault();
      const { pool, serverID } = this.props;
      this.props.setAllocation(pluginRegistry.getServer(serverID), pool, this.state.alloc, this.state.exportType);
      this.toggleAllocation();
    };

    this.allocChange = evt => {
      this.setState({ alloc: evt.target.value });
    };

    this.toggleConfirmDelete = evt => {
      this.setState({ isConfirmDeleteOpen: !this.state.isConfirmDeleteOpen });
    };

    this.trashRegion = evt => {
      const { serverID, pool, deleteAPool } = this.props;
      const serverObject = pluginRegistry.getServer(serverID);
      this.toggleConfirmDelete();
      ContextMenu.hide();
      deleteAPool(serverObject, pool);
      this.props.history.push(`/number-range/pools/${serverObject.serverID}`);
    };

    this.handleExportChange = evt => {
      this.setState({ exportType: evt.target.value });
    };

    this.state = {
      isAllocationOpen: false,
      alloc: 0,
      isConfirmDeleteOpen: false,
      exportType: "json"
    };
  }

  renderContextMenu() {
    const { serverID, pool } = this.props;
    const { sequential, randomized, listBased } = this.getAllowedRegionTypes();
    return React.createElement(
      Menu,
      null,
      React.createElement(
        ButtonGroup,
        { className: "context-menu-control", minimal: true },
        React.createElement(Button, { small: true, onClick: this.goToEdit, iconName: "edit" }),
        React.createElement(Button, {
          small: true,
          onClick: this.toggleConfirmDelete,
          iconName: "trash"
        })
      ),
      React.createElement(MenuDivider, { title: pool.readable_name }),
      React.createElement(MenuDivider, null),
      sequential ? React.createElement(MenuItem, {
        onClick: this.goTo.bind(this, `/number-range/add-region/${serverID}/${pool.machine_name}`),
        text: `${this.props.intl.formatMessage({
          id: "plugins.numberRange.addSequentialRegion"
        })}`
      }) : null,
      randomized ? React.createElement(MenuItem, {
        onClick: this.goTo.bind(this, `/number-range/add-randomized-region/${serverID}/${pool.machine_name}`),
        text: `${this.props.intl.formatMessage({
          id: "plugins.numberRange.addRandomizedRegion"
        })}`
      }) : null,
      listBased ? React.createElement(MenuItem, {
        onClick: this.goTo.bind(this, `/number-range/add-list-based-region/${serverID}/${pool.machine_name}`),
        text: `${this.props.intl.formatMessage({
          id: "plugins.numberRange.addListBasedRegion"
        })}`
      }) : null,
      React.createElement(MenuItem, {
        onClick: this.toggleAllocation,
        text: this.props.intl.formatMessage({
          id: "plugins.numberRange.allocateButton"
        })
      })
    );
  }

  render() {
    const { pool, serverID, key } = this.props;
    return React.createElement(
      TreeNode,
      {
        onContextMenu: this.renderContextMenu.bind(this),
        key: key,
        path: `/number-range/region-detail/${serverID}/${pool.machine_name}`,
        nodeType: "pool",
        active: this.state.active,
        collapsed: this.state.collapsed,
        depth: this.props.depth,
        childrenNodes: [] },
      React.createElement(
        Dialog,
        {
          isOpen: this.state.isAllocationOpen,
          onClose: this.toggleAllocation,
          title: `${this.props.intl.formatMessage({
            id: "plugins.numberRange.allocateButton"
          })} ${pool.readable_name}`,
          className: classNames({
            "pt-dark": this.props.theme.startsWith("dark") ? true : false
          }) },
        React.createElement(
          "div",
          { className: "pt-dialog-body" },
          React.createElement(
            "form",
            { onSubmit: this.setAllocation, className: "mini-form" },
            React.createElement("input", {
              placeholder: "allocate",
              className: "pt-input",
              type: "number",
              defaultValue: 1,
              value: this.state.alloc,
              onChange: this.allocChange,
              min: 1,
              max: Number(pool.request_threshold),
              style: { width: 200 }
            }),
            React.createElement(
              "div",
              { style: { marginTop: "30px", marginBottom: "20px" } },
              React.createElement(
                RadioGroup,
                {
                  inline: true,
                  label: "Export Type",
                  onChange: this.handleExportChange,
                  selectedValue: this.state.exportType },
                React.createElement(Radio, { label: "JSON", value: "json" }),
                React.createElement(Radio, { label: "CSV", value: "csv" }),
                React.createElement(Radio, { label: "XML", value: "xml" })
              )
            ),
            React.createElement(
              "button",
              { type: "submit", className: "pt-button" },
              React.createElement(FormattedMessage, { id: "plugins.numberRange.allocateButton" })
            )
          )
        )
      ),
      React.createElement(DeleteDialog, {
        isOpen: this.state.isConfirmDeleteOpen,
        title: React.createElement(FormattedMessage, {
          id: "plugins.numberRange.deleteRegion",
          values: { regionName: pool.readable_name }
        }),
        body: React.createElement(FormattedMessage, { id: "plugins.numberRange.deleteRegionConfirm" }),
        toggle: this.toggleConfirmDelete.bind(this),
        deleteAction: this.trashRegion.bind(this)
      }),
      pool.readable_name
    );
  }
}

const PoolItem = connect(state => {
  return {
    currentRegions: state.numberrange.currentRegions,
    servers: state.serversettings.servers,
    currentPath: state.layout.currentPath,
    theme: state.layout.theme
  };
}, { setAllocation: _numberrange.setAllocation, deleteAPool: _numberrange.deleteAPool })(withRouter(_PoolItem));

const NavItems = exports.NavItems = (pools, serverID, intl) => {
  if (!Array.isArray(pools)) {
    return [];
  }
  return pools.map(pool => {
    // passing intl down to use formatMessage and translate...
    return React.createElement(PoolItem, { key: pool.name, pool: pool, serverID: serverID, intl: intl });
  });
};

class _NavPluginRoot extends Component {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.goTo = path => {
      this.props.history.push(path);
    }, this.renderContextMenu = () => {
      const { server, serverID } = this.props;
      return React.createElement(
        Menu,
        null,
        React.createElement(MenuDivider, { title: server.serverSettingName }),
        React.createElement(MenuDivider, null),
        React.createElement(MenuItem, {
          onClick: this.goTo.bind(this, `/number-range/add-pool/${serverID}`),
          text: this.props.intl.formatMessage({
            id: "plugins.numberRange.addPool"
          })
        })
      );
    }, _temp;
  }

  static get PLUGIN_COMPONENT_NAME() {
    return "NumberRangeNavRoot";
  }
  serverHasSerialbox() {
    return pluginRegistry.getServer(this.props.serverID).appList.includes("serialbox");
  }

  componentDidMount() {
    if (this.props.server && this.serverHasSerialbox()) {
      this.props.loadPools(pluginRegistry.getServer(this.props.serverID));
    }
  }

  render() {
    const { serverID, pools } = this.props;
    if (this.props.server && this.serverHasSerialbox()) {
      let children = NavItems(pools, serverID, this.props.intl);
      return React.createElement(
        TreeNode,
        {
          serverID: serverID,
          onContextMenu: this.renderContextMenu,
          nodeType: "plugin",
          depth: this.props.depth,
          childrenNodes: children,
          path: `/number-range/pools/${serverID}` },
        React.createElement(FormattedMessage, { id: "plugins.numberRange.navItemsTitle" })
      );
    } else {
      return React.createElement(
        TreeNode,
        { depth: this.props.depth, childrenNodes: [] },
        React.createElement(
          "i",
          null,
          React.createElement(FormattedMessage, { id: "plugins.numberRange.noNumberRangeFound" })
        )
      );
    }
  }
}

exports._NavPluginRoot = _NavPluginRoot;
const NavPluginRoot = exports.NavPluginRoot = connect((state, ownProps) => {
  return {
    server: state.serversettings.servers[ownProps.serverID],
    pools: state.numberrange.servers && state.numberrange.servers[ownProps.serverID] ? state.numberrange.servers[ownProps.serverID].pools : [],
    currentPath: state.layout.currentPath
  };
}, { loadPools: _numberrange.loadPools })(withRouter(_NavPluginRoot));