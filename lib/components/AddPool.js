"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AddPool = undefined;

var _PoolForm = require("./PoolForm");

var _PoolForm2 = _interopRequireDefault(_PoolForm);

var _numberrange = require("../reducers/numberrange");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

const React = qu4rtet.require("react");
const { Component } = React;
const { connect } = qu4rtet.require("react-redux");
const { RightPanel } = qu4rtet.require("./components/layouts/Panels");
const { Card, Button, ButtonGroup } = qu4rtet.require("@blueprintjs/core");
const { FormattedMessage } = qu4rtet.require("react-intl");

class _AddPool extends Component {
  constructor(props) {
    super(props);

    this.editResponseRule = responseRule => {
      let pool = this.getPool();
      this.props.history.push({
        pathname: `/number-range/add-response-rule/${this.currentServer.server.serverID}/pool-id/${pool.id}`,
        state: { defaultValues: responseRule, edit: true, pool: pool }
      });
    };

    this.deleteResponseRule = responseRule => {
      let pool = this.getPool();
      this.props.deleteResponseRule(this.currentServer.server, responseRule);
    };

    this.getEditMode = () => {
      return this.props.location && this.props.location.state && this.props.location.state.editPool || this.props.match.params.poolName ? true : false;
    };

    this.getPool = () => {
      let pool = {};
      let editMode = this.getEditMode();
      if (editMode) {
        try {
          pool = this.props.location.state.defaultValues;
        } catch (e) {
          // location state may not be available.
        }
      }
      if (this.props.match.params.poolName) {
        editMode = true;
        let pools = this.props.nr[this.currentServer.server.serverID].pools;
        // most up to date.
        pool = pools.find(pool => {
          return pool.machine_name === this.props.match.params.poolName;
        });
      }
      return pool;
    };

    this.currentServer = this.props.nr[this.props.match.params.serverID];
  }
  componentDidMount() {}

  render() {
    let editMode = this.getEditMode();
    let pool = this.getPool();
    return React.createElement(
      RightPanel,
      {
        title: !editMode ? React.createElement(FormattedMessage, { id: "plugins.numberRange.addPool" }) : React.createElement(FormattedMessage, { id: "plugins.numberRange.editPool" }) },
      React.createElement(
        "div",
        { className: "large-cards-container" },
        React.createElement(
          Card,
          { className: "pt-elevation-4 form-card" },
          React.createElement(
            "h5",
            null,
            !editMode ? React.createElement(FormattedMessage, { id: "plugins.numberRange.addPool" }) : React.createElement(FormattedMessage, { id: "plugins.numberRange.editPool" })
          ),
          React.createElement(_PoolForm2.default, {
            server: this.currentServer.server,
            history: this.props.history,
            pool: pool
          })
        ),
        editMode ? React.createElement(
          Card,
          { className: "pt-elevation-4 form-card" },
          React.createElement(
            "h5",
            null,
            React.createElement(
              "button",
              {
                className: "pt-button right-aligned-elem pt-interactive pt-intent-primary",
                onClick: e => {
                  this.props.history.push({
                    pathname: `/number-range/add-response-rule/${this.currentServer.server.serverID}/pool-id/${pool.id}/`,
                    state: { pool: pool }
                  });
                } },
              React.createElement(FormattedMessage, { id: "plugins.numberRange.addResponseRule" })
            ),
            React.createElement(FormattedMessage, { id: "plugins.numberRange.responseRules" })
          ),
          pool && Array.isArray(pool.response_rules) && pool.response_rules.length > 0 ? React.createElement(
            "table",
            { className: "pt-table pt-interactive pt-bordered pt-striped" },
            React.createElement(
              "thead",
              null,
              React.createElement(
                "tr",
                null,
                React.createElement(
                  "th",
                  null,
                  React.createElement(FormattedMessage, {
                    id: "plugins.numberRange.ruleName",
                    defaultMessage: "Rule Name"
                  })
                ),
                React.createElement(
                  "th",
                  null,
                  " ",
                  React.createElement(FormattedMessage, {
                    id: "plugins.numberRange.contentType",
                    defaultMessage: "Content Type"
                  })
                ),
                React.createElement("th", null)
              )
            ),
            React.createElement(
              "tbody",
              null,
              pool.response_rules ? pool.response_rules.map(responseRule => {
                return React.createElement(
                  "tr",
                  { key: responseRule.id },
                  React.createElement(
                    "td",
                    null,
                    responseRule.rule ? this.props.rules.find(rule => rule.id === responseRule.rule).name : null
                  ),
                  React.createElement(
                    "td",
                    null,
                    responseRule.content_type
                  ),
                  React.createElement(
                    "td",
                    { style: { width: "80px" } },
                    React.createElement(
                      ButtonGroup,
                      { minimal: true, small: true },
                      React.createElement(Button, {
                        small: "true",
                        iconName: "edit",
                        onClick: this.editResponseRule.bind(this, responseRule)
                      }),
                      React.createElement(Button, {
                        small: "true",
                        iconName: "trash",
                        onClick: this.deleteResponseRule.bind(this, responseRule)
                      })
                    )
                  )
                );
              }) : null
            )
          ) : null
        ) : null
      )
    );
  }
}

const AddPool = exports.AddPool = connect((state, ownProps) => {
  return {
    servers: state.serversettings.servers,
    nr: state.numberrange.servers,
    rules: state.capture.servers ? state.capture.servers[ownProps.match.params.serverID].rules : []
  };
}, { deleteResponseRule: _numberrange.deleteResponseRule })(_AddPool);